Distributed Nabla Semi-Supervised Support Vector Machine

Description
-------
This library implements a distributed training algorithm for semi-supervised
support vector machine (S3VM) [1]. Specifically, starting from the smooth
approximation presented in [2], we provide two different distributed strategies
based on gradient diffusion [3] and successive convex approximations, respectively.


Citation
--------
If you use this code or any derivative thereof in your research, we would
appreciate if you cite the following publication:

@article{scardapane2016distributed,
  title={Distributed semi-supervised support vector machines},
  author={Scardapane, Simone and Fierimonte, Roberto and Di Lorenzo, Paolo and Panella, Massimo and Uncini, Aurelio},
  journal={Neural Networks},
  year={2016},
  publisher={Elsevier},
  doi={10.1016/j.neunet.2016.04.007}
}


Usage 
-------
To launch a simulation, simply use the script 'test_script.m'. All the
configuration parameters are specified in the script 'params_selection'.
Currently, 5 algorithms are compared:

   * Linear SVM implemented with LibSVM.
   * Kernel SVM using a Gaussian kernel (also with LibSVM).
   * Nabla S3VM solved using a standard gradient descent procedure.
   * Distributed Nabla S3VM implemented using the two different strategies.

Note: the code comes with version 3.20 of LibSVM, for any information on its 
use and/or compilation please refer to:
https://www.csie.ntu.edu.tw/~cjlin/libsvm/


Licensing
---------
The code is distributed under BSD-2 license. Please see the file called LICENSE.

The code uses several utility functions from MATLAB Central. Copyright
information and licenses can be found in the 'functions' folder.

Network topology in folder 'network' is adapted from the Lynx MATLAB toolbox:
https://github.com/ispamm/Lynx-Toolbox


Contacts
--------

   o If you have any request, bug report, or inquiry, you can contact
     the author at simone [dot] scardapane [at] uniroma1 [dot] it.
   o Additional contact information can also be found on the website of
     the author:
	      http://ispac.ing.uniroma1.it/scardapane/


References
--------
[1] Chapelle, O., Sindhwani, V., & Keerthi, S. S. (2008). Optimization 
    techniques for semi-supervised support vector machines. Journal of 
    Machine Learning Research, 9, 203-233.
[2] Chapelle, O., & Zien, A. (2005). Semi-supervised classification by low 
    density separation. In Proceedings of the tenth international workshop 
    on artificial intelligence and statistics (Vol. 1, pp. 57-64).
[3] Chen, J., & Sayed, A. H. (2012). Diffusion adaptation strategies for 
    distributed optimization and learning over networks. IEEE Transactions 
    on Signal Processing, 60(8), 4289-4305.