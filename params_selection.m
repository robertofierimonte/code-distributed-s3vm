% -------------------------------------------------------------------------
% --- PARAMS_SELECTION ----------------------------------------------------
% --- Define the parameters for the simulation ----------------------------
% -------------------------------------------------------------------------

% Params of the NS3VM model
C_l = 1;                    % Value of the soft-margin parameter for labeled data
C_u = 1;                    % Value of the soft-margin parameter for unlabeled data
s = 5;                      % Value of the NS3VM smoothing parameter

% Params of the gradient descend procedure for NS3VM
maxIters = 500;             % Maximum number of iterations for the gradient descent procedure
alpha0 = 1;                 % Initial step-size value for the gradient descent procedure
delta = 0.55;               % Value of the step-size vanishing coefficient for the gradient descent procedure
threshold = 10^-5;          % Value of the threshold (termination criteria) for the gradient descent procedure

% Params of the diffused grdient algorithm for Distr-NS3VM
distrMaxIters = 500;        % Maximum number of iterations for the diffused gradient algorithm
distrAlpha0 = 1;            % Initial step-size value for the diffused gradient algorithm
distrDelta = 0.55;          % Value of the step-size vanishing coefficient for the diffused gradient algorithm

% Params of the NEXT algorithm for Distr-NS3VM
nextMaxIters = 500;         % Maximum number of iterations for NEXT
nextAlpha0 = 0.6;           % Initial step-size value for NEXT
nextDelta = 0.8;            % Value of the step-size vanishing coefficient for NEXT

% Params of the internal solver for NEXT
internMaxIters = 50;        % Maximum number of iterations for the internal solver
internAlpha0 = 0.5;         % Initial step-size value for the internal solver
internDelta = 0.7;          % Value of the step-size vanishing coefficient for the internal solver
internThreshold = 10^-5;    % Value of the threshold (termination criteria) for the internal solver

% Dataset configuration
datasetFile = 'garageband.mat';   % Dataset to load
runs = 1;                   % Number of simulations
kfolds = 10;                % Number of folds

% Network configuration
agents = [5 25];            % Nodes in the network (can be a vector for testing multiple sizes simultaneously)
connectivity = 0.25;        % Connectivity in the networks (must be between 0 and 1)

load(datasetFile);
if strcmp(name, 'g50c')
    
    dataset = Dataset(name, X, task, Y);    % Load and preprocess dataset Data
    dataset = normalize(dataset, -1, 1);
    
    % Sizes of the training set
    nTrain = 40;                % Size of the labeled training set
    nUnsupervised = 400;        % Size of the unlabeled training set

elseif strcmp(name, 'garageband')
    
    dataset = Dataset(name, X, task, Y);    % Load and preprocess dataset Data
    dataset = normalize(dataset, -1, 1);
    
    % Custom parameters for GARAGEBAND
    C_l = 10; 
    C_u = 1;
    
    alpha0 = 0.05;
    distrAlpha0 = 0.55;
    nextAlpha0 = 0.55;
    nextDelta = 0.55;
    %internAlpha0 = 0.05;         % Initial step-size value for the internal solver
    
    % Sizes of the training set
    nTrain = 40;                % Size of the labeled training set
    nUnsupervised = 671;        % Size of the unlabeled training set
    
elseif strcmp(name, 'COIL20(B)')
    dataset = Dataset(name, X, task, Y);    % Load and preprocess dataset Data
    dataset = normalize(dataset, -1, 1);
    
    % Custom parameters for COIL20(B)
    C_l = 10;
    C_u = 1;
    
    alpha0 = 0.001;
    delta = 0.9;
    
    distrAlpha0 = 0.005;
    distrDelta = 0.55;
    
    % Sizes of the training set
    nTrain = 40;                % Size of the labeled training set
    nUnsupervised = 1250;        % Size of the unlabeled training set
    
elseif strcmp(name, 'pcmac')
    dataset = Dataset(name, X(1:1940,:), task, Y(1:1940));    % Load and preprocess dataset Data
    
    % Custom parameters for PCMAC
    % Params of the DS3VM model
    C_l = 100;                    % Value of the soft-margin parameter for labeled data
    C_u = 100;                    % Value of the soft-margin parameter for unlabeled data
    
    % Sizes of the training set
    nTrain = 40;                % Size of the labeled training set
    nUnsupervised = 1700;       % Size of the unlabeled training set
    
    % Params of the gradient descend procedure for NS3VM
    delta = 0.9;                % Value of the step-size vanishing coefficient for the gradient descent procedure
    
    % Params of the diffused grdient algorithm for Distr-NS3VM
    distrMaxIters = 500;        % Maximum number of iterations for the diffused gradient algorithm
    distrAlpha0 = 1;            % Initial step-size value for the diffused gradient algorithm
    distrDelta = 0.9;           % Value of the step-size vanishing coefficient for the diffused gradient algorithm
    
elseif strcmp(name, 'USPS(B)')
    dataset = Dataset(name, X(1:2000,:), task, Y(1:2000));    % Load and preprocess dataset Data
    dataset = normalize(dataset, -1, 1);
    
    % Custom parameters for USPS(B)
    C_l = 1;
    C_u = 0.1;
    
    alpha0 = 0.005;
    delta = 0.1;
    
    % Sizes of the training set
    nTrain = 40;                % Size of the labeled training set
    nUnsupervised = 1950;       % Size of the unlabeled training set
    
end