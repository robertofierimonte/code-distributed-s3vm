% -------------------------------------------------------------------------
% --- TEST_SCRIPT ---------------------------------------------------------
% --- Run the overall simulation ------------------------------------------
% -------------------------------------------------------------------------

% Initialize workspace, pseudo-random number generator and folders on path
clear all; 
clc; rng(1);
f = genpath(pwd); addpath(f);

% Preprocess dataset and select parameters for the simulation
params_selection;

% Split the dataset for N different times
dataset = dataset.generateNPartitions(runs, KFoldPartition(kfolds), ExactPartition(nTrain, nUnsupervised));

% Error function (MSE)
errFcn = @(d,y) sum(d ~= y)/length(d);  

% Initialize the internal solvers (the first one is for the centralized
% Nabla-SVM, the second one for solving the internal optimization problem
% in the NEXT algorithm
gdSolver = GradientDescentSolver('maxIters', maxIters, 'alpha0', alpha0, 'delta', delta, 'threshold', threshold);
internalGdSolver = GradientDescentSolver('maxIters', internMaxIters, 'alpha0', internAlpha0, 'delta', internDelta, 'threshold', internThreshold);

% Define the centralized algorithms
centralized_algorithms = {...
    CentralizedSVM('LIN-SVM', 1, '0', ''), ...
    CentralizedSVM('RBF-SVM', 1, '2', ''), ...
    NablaS3VM('NS3VM (GD)', C_l, C_u, s, gdSolver), ...
};

% Define the distributed algorithms
distributed_algorithms = {...
    DiffusionNablaS3VM('D-NS3VM', C_l, C_u, s, 'maxIters', distrMaxIters, 'alpha0', distrAlpha0, 'delta', distrDelta);
    DiffusionNablaS3VM('NEXT-NS3VM', C_l, C_u, s, 'maxIters', nextMaxIters, 'trainAlg', 'next', 'alpha0', nextAlpha0, 'delta', nextDelta, 'internalSolver', internalGdSolver),
};

% -----------------------------
% --- MAIN SIMULATION ---------
% -----------------------------

% Initialize output matrices
N_algorithms = length(centralized_algorithms) + length(distributed_algorithms)*length(agents);
errors = zeros(runs*kfolds, N_algorithms);
time = zeros(runs*kfolds, N_algorithms);

% Structures for saving internal evaluations of objective function and
% gradient norm
trainInfo = cell(runs*kfolds, N_algorithms);

% Initialize the networks' topologies
nets = cell(length(agents), 1);
for ii = 1:length(agents)
    nets{ii} = RandomTopology(agents(ii), 'metropolis', connectivity);
end

z = 1;  % Auxiliary index for the output structures

for n = 1:runs
    
    fprintf('--- RUN %i/%i ---\n', n, runs);
    
    % Set the current partition in the dataset
    dataset = dataset.setCurrentPartition(n);
    
    for k = 1:kfolds
       
        % Split and distribute the data
        [trainData, testData, unlabeledData] = dataset.getFold(k);
        trainDataDistributed = cell(length(agents), 1);
        unlabeledDataDistributed = cell(length(agents), 1);
        for ii = 1:length(agents)
            if(agents(ii) > 1)
                distrPartition = KFoldPartition(agents(ii));
            else
                distrPartition = NoPartition();
            end
            trainDataDistributed{ii} = trainData.distributeDataset(distrPartition);
            unlabeledDataDistributed{ii} = unlabeledData.distributeDataset(distrPartition);
        end
        
        fprintf('\tFold %i/%i - %i training, %i test, %i unlabeled\n', k, kfolds, size(trainData.X, 1), size(testData.X, 1), size(unlabeledData.X, 1));
        
        % Test the centralized algorithms
        for a = 1:length(centralized_algorithms)
            fprintf('\t\tTraining %s...\n', centralized_algorithms{a}.name);
            tic;
            [centralized_algorithms{a}, trainInfo{z,a}] = ...
                centralized_algorithms{a}.train(trainData, unlabeledData);
            time(z, a) = toc;
            errors(z, a) = errFcn(testData.Y, centralized_algorithms{a}.test(testData));
        end
        
        % Correct for the index
        if isempty(centralized_algorithms)
            a = 0;
        end
        a = a + 1;
        
        % Test the distributed algorithms
        for b = 1:length(distributed_algorithms)
            for ii = 1:length(agents)
                L = agents(ii);
                fprintf('\t\tTraining %s (%i agents)...\n', distributed_algorithms{b}.name, L);
                tic;
                [distributed_algorithms{b}, trainInfo{z,a}] = ...
                    distributed_algorithms{b}.train(nets{ii}, trainDataDistributed{ii}, unlabeledDataDistributed{ii});
                time(z, a) = toc./L;
                errors(z, a) = errFcn(testData.Y, distributed_algorithms{b}.test(testData));
                a = a + 1;
            end
        end
        
        z = z + 1;
    end
    
end

%% -----------------------------
% --- OUTPUT-------------------
% -----------------------------

fprintf('-----------------------\n');
fprintf('--- RESULTS -----------\n');
fprintf('-----------------------\n');

% Collect the names of the algorithms (in the output, there is one
% algorithm for each tested size of the network)
names = cell(N_algorithms, 1);
for i=1:length(centralized_algorithms)
    names{i} = centralized_algorithms{i}.name;
end
if(isempty(i))
    i = 1;
else
    i = i + 1;
end
for j=1:length(distributed_algorithms)
    for z=1:length(agents)
        names{i} = [distributed_algorithms{j}.name, ' (', num2str(agents(z)), ')'];
        i = i + 1;
    end
end

% Show the output tables and plots
disptable([mean(errors,1)' mean(time,1)'], {'Tst. error', 'Tr. times'}, names); 
make_plots;