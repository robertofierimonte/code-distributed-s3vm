classdef GradientDescentSolver
    % GRADIENTDESCENTSOLVER - Gradient descent procedure for optimizing a
    % generic function
    properties
        threshold;  % Threshold for the gradient norm
        maxIters;   % Maximum number of iterations
        alpha0;     % Initial step size
        delta;      % Exponent for the step size evolution
    end
    
    methods
        
        function obj = GradientDescentSolver(varargin)
            p = inputParser;
            p.KeepUnmatched = true;
            p.addParamValue('threshold', 10^-5);
            p.addParamValue('maxIters', 500);
            p.addParamValue('alpha0', 1);
            p.addParamValue('delta', 0.55);
            p.parse(varargin{:});
            obj.threshold = p.Results.threshold;
            obj.maxIters = p.Results.maxIters;
            obj.alpha0 = p.Results.alpha0;
            obj.delta = p.Results.delta;
        end
        
        function [woptimal, info] = solve(obj, fcn, grad_fcn, w0)
            % Input arguments:
            %   - fcn: function to be minimized (only for debug purposes,
            %   can be empty).
            %   - grad_fcn: gradient of the function to be minimized.
            %   - w0: initial starting point.
            %
            % Output arguments:
            %   - woptimal: optimal value found by the procedure.
            %   - info: structure with debug information, namely 'fval' is
            %   a vector with function evaluations (only if 'fcn' is not
            %   empty), and gval a vector with gradient evaluations.
            fval = zeros(obj.maxIters,1);
            gval = zeros(obj.maxIters,1);
            
            wCurr = w0;
            
            currentGrad = Inf;
            for ii = 1:obj.maxIters
                if(norm(currentGrad) <= obj.threshold)
                    break;
                end
                
                % Evaluate objective function and gradient
                if(~isempty(fcn))
                    fval(ii) = fcn(wCurr);
                end
                
                currentGrad = grad_fcn(wCurr);
                gval(ii) = norm(currentGrad).^2;
                
                % Perform one gradient descent step
                alpha = obj.alpha0/(ii)^obj.delta;
                wCurr = wCurr - alpha*currentGrad;
                
                
            end
            
            woptimal = wCurr;
            
            info.fval = fval;
            info.gval = gval;
            
        end
        
    end
    
end

