classdef SCANablaS3VM < LearningAlgorithm
    % NablaS3VM solved using successive convex approximation
    % Requires internal CVX solver, only for debug purposes.
    % TODO: replace CVX solver with GradientDescentSolver or 
    % ConjugateGradientSolver.
    
    properties
        w;          % Linear coefficients of the optimal hyperplane
        b;          % Intercept
        threshold;  % Threshold
        maxIters;   % Maximum number of iterations
        alpha0;     % Initial step size
        delta;      % Exponent for the step size evolution
    end
    
    methods
        function obj = SCANablaS3VM(name, C_l, C_u, s, varargin)
            obj = obj@LearningAlgorithm(name);
            obj.params.C_l = C_l;
            obj.params.C_u = C_u;
            obj.params.s = s;
            p = inputParser;
            p.addParameter('threshold', 10^-5);
            p.addParameter('maxIters', 50);
            p.addParameter('alpha0', 0.5);
            p.addParameter('delta', 0.55);
            p.parse(varargin{:});
            obj.threshold = p.Results.threshold;
            obj.maxIters = p.Results.maxIters;
            obj.alpha0 = p.Results.alpha0;
            obj.delta = p.Results.delta;
        end
        
        function [obj, trainInfo] = train(obj, trainData, unlabeledData)
            
            d = size(trainData.X, 2);
            Nlabeled = size(trainData.X, 1);
            
            fval = zeros(obj.maxIters, 1);
            gval = zeros(obj.maxIters, 1);
            
            wCurr = zeros(d, 1);
            
            unlabeledData = translateUnlabeled(unlabeledData);
            
            % Compute the offset
            obj.b = mean(trainData.Y);

            for n = 1:obj.maxIters
                
                alpha = obj.alpha0/(n)^obj.delta;
                
                % Compute objective functions and gradient norms
                % (for evaluation purposes)
                fval(n) = obj.computeGlobalObjFunction(trainData, unlabeledData, wCurr);
                [grad, ~, grad_g] = obj.computeGradient(trainData, unlabeledData, wCurr);
                gval(n) = norm(grad)^2;
                
                Cl = obj.params.C_l;

                cvx_begin quiet
                    variable w_hat(d,1);
                    minimize((Cl/Nlabeled)*sum_square_pos(1-trainData.Y.*(trainData.X*w_hat + obj.b)) + grad_g'*w_hat + 0.5*w_hat'*w_hat );
                cvx_end
                
                dir = (w_hat - wCurr);
                wCurr = wCurr + alpha*dir;
                
                % Check termination condition
                if (gval(n) <= obj.threshold)
                    break;
                end
                
            end

            obj.w = wCurr;
            
            trainInfo.fval = fval;
            trainInfo.gval = gval;
            
        end
        
        
        function [grad, grad_f, grad_g, grad_h] = computeGradient(obj, trainData, unlabeledData, w)
            
            Nlabeled = size(trainData.X, 1);
            Nunlabeled = size(unlabeledData.X, 1);
            
            yfx = trainData.Y.*(trainData.X*w + obj.b);
            aux_coefficient = (yfx < 1).*trainData.Y.*(1-yfx);

            y_unlabeled = unlabeledData.X*w + obj.b;
            aux2 = exp(-obj.params.s*y_unlabeled.^2).*y_unlabeled;
            
            grad_f = -2*(obj.params.C_l/Nlabeled)*sum(repmat(aux_coefficient, 1, size(trainData.X, 2)).*trainData.X, 1)';
            grad_g = -2*obj.params.s*(obj.params.C_u/Nunlabeled)*sum(unlabeledData.X.*repmat(aux2, 1,size(trainData.X,2)), 1)';
            grad_h = w;
            
            grad = grad_f + grad_g + grad_h;
            
        end
        
        function func = computeGlobalObjFunction(obj, trainData, unlabeledData, w)
            
            y_labeled = trainData.X*w + obj.b;
            y_unlabeled = unlabeledData.X*w + obj.b;
            
            aux = y_labeled.*trainData.Y;
            func = (norm(w)^2)/2 + (obj.params.C_l/size(y_labeled, 1))*sum(max(0, 1 - aux).^2) + (obj.params.C_u/size(y_unlabeled, 1))*sum(exp(-obj.params.s*y_unlabeled.^2));
            
        end

        function [labels, scores] = test(obj, testData)
            
            labels = sign(testData.X*obj.w + obj.b);
            scores = labels;
            
        end
    end
    
end