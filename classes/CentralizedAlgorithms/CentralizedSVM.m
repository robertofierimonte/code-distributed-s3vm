classdef CentralizedSVM < LearningAlgorithm
    % Standard C-SVM implemented using LibSVM
    
    properties
        Xtr;                % Training samples
        C;                  % Regularization coefficient
        kernel_type;        % Kernel type (integer)
        kernel_parameters;  % Kernel parameter (string of parameters in LibSVM format)
        svmStruct;          % Resulting SVM structure as returned by the training function
    end
    
    methods
        function obj = CentralizedSVM(name, C, kernel_type, kernel_parameters)
            obj = obj@LearningAlgorithm(name);
            obj.C = C;
            obj.kernel_type = kernel_type;
            obj.kernel_parameters = kernel_parameters;
        end
        
        function [obj, trainInfo] = train(obj, trainData, ~)
           
            % Get training data
            obj.Xtr = trainData.X;
            Ytr = trainData.Y;
            
            % Construct the option string for LibSVM
            options = ['-t ', num2str(obj.kernel_type), ' ', obj.kernel_parameters, ' -c ',  num2str(obj.C), ' -h 0 -q 1'];
            
            % Train the SVM
            obj.svmStruct = svmtrain2(Ytr, obj.Xtr, options);
            
            trainInfo = struct();
            
        end
        
        function [labels, scores] = test(obj, testData)
            % Test the SVM
            [labels, ~, scores] = svmpredict(zeros(size(testData.X, 1),1), testData.X, obj.svmStruct, '-q 1');
        end
    end
    
end

