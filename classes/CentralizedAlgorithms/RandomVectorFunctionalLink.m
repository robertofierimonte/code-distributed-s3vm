% RandomVectorFunctionalLink - Random vector functional link network
%   The RVFL network was introduced in the following works:
%
%   [1] Pao, Y. H., Park, G. H., & Sobajic, D. J. (1994). Learning and 
%   generalization characteristics of the random vector functional-link 
%   net. Neurocomputing, 6(2), 163-180.
%   [2] Igelnik, B., & Pao, Y. H. (1995). Stochastic choice of basis 
%   functions in adaptive function approximation and the functional-link
%   net. IEEE Transactions on Neural Networks, 6(6), 1320-1329.
%
%   Recently, an equivalent model was introduced under the name of Extreme
%   Learning Machine (ELM). This class provides a least-square training
%   algorithm for RVFL networks with sigmoid non-linearity.
%
%   The code is adapted from the Lynx MATLAB Toolbox:
%   https://github.com/ispamm/Lynx-Toolbox
%
%   The original code was used for a series of recent publications,
%   including:
%
%   [3] Scardapane, S., Comminiello, D., Scarpiniti, M., & Uncini, A. 
%   (2015). Online Sequential Extreme Learning Machine With Kernels, IEEE
%   Transactions on Neural Networks and Learning Systems, 26(9), 2212-2220.
%   [4] Scardapane, S., Wang, D., Panella, M. & Uncini, A. (2015). 
%   Distributed Learning for Random Vector Functional-Link Networks. 
%   Information Sciences, 301, 271-284. 
%
%   More information on the original publications can be found on the
%   author's website:
%   http://ispac.diet.uniroma1.it/scardapane/software/code/
%
%   To use the code, you can initialize the model by providing a number of
%   hidden neurons and a regularization factor:
%
%       net = RandomVectorFunctionalLink(neurons, lambda);
%
%   Then, to train the net:
%
%       net = net.train(X, Y);
%
%   Where X is an NxD matrix, with N = number of observations and D =
%   number of features, while Y is the corresponding output matrix of size
%   NxM, where M = number of desired outputs.
%
%   Once the net is trained, you can get predictions with:
%
%       net = net.test(X);
%
%   where X has a similar structure with respect to the one used for
%   training.

classdef RandomVectorFunctionalLink < LearningAlgorithm
    
    properties
        
        % Weights connecting to the hidden layer
        weights_l1;
        
        % Biases of the hidden layer
        bias_l1;
        
        % Weights connecting to the hidden layer
        outputWeights;
        
        % Regularization coefficient
        lambda;
        
        % Number of hidden neurons
        B;
        
    end
    
    methods
        
        function obj = RandomVectorFunctionalLink(name, neurons, lambda)
            obj = obj@LearningAlgorithm(name);
            obj.B = neurons;
            obj.lambda = lambda;
        end
        
        function [obj, info] = train(obj, trainData, ~)
            
            Xtr = trainData.X;
            Ytr = trainData.Y;
            
            info = struct();
            
            [N, d] = size(Xtr);
            
            N_hidden = obj.B;
            
            % Generate the stochastic matrices
            obj = obj.generateWeights(d);
        
            % Compute the hidden matrix
            H = obj.computeHiddenMatrix(Xtr);
            
            % Compute the output weights via regularized least-square
            % regression
            
            if(N >= N_hidden)
                obj.outputWeights = (eye(N_hidden)./obj.lambda + H' * H) \ ( H' * Ytr );
            else
                obj.outputWeights = H'*inv(eye(size(H, 1))./obj.lambda + H * H') *  Ytr ;
            end

        end
        
        function [labels, scores] = test(obj, testData)
            
            % Compute hidden matrix
            H_temp_test = obj.computeHiddenMatrix(testData.X);
            
            % Get the predictions
            scores =(H_temp_test * obj.outputWeights);
            labels = sign(scores);
            
        end
        
        function obj = generateWeights(obj, d)
            N_hidden = obj.B;
            if(~isempty(obj.weights_l1) && size(obj.weights_l1, 1) == N_hidden)
                % Weights are already initialized
            else
                % Generate randomly the weights
                obj.weights_l1 = rand(N_hidden, d)*2-1;
                obj.bias_l1 = rand(N_hidden,1)*2-1;
            end
        end
        
        function H = computeHiddenMatrix(obj, X)
            % Compute the hidden matrix
            H = obj.weights_l1*X';
            H = bsxfun(@plus, H, obj.bias_l1);
            H = 1 ./ (1 + exp(-H));
            H = H';
        end
        
    end
    
end

