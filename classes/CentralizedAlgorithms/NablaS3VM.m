classdef NablaS3VM < LearningAlgorithm
    % NABLASVM - Semi-supervised SVM implemented using the standard
    % Nabla-SVM approximation, e.g. see:
    %
    %  [1] Chapelle, O., & Zien, A. (2005, January). Semi-supervised 
    %  classification by low density separation. In Proceedings of the 
    %  tenth international workshop on artificial intelligence and 
    %  statistics (Vol. 1, pp. 57-64).
    %
    % The internal optimization problem is solved using a standard gradient
    % descent procedure.
    
    properties
        w;          % SVM linear coefficients
        b;          % SVM offset
        solver;     % Internal gradient descent solver
    end
    
    methods
        
        function obj = NablaS3VM(name, C_l, C_u, s, solver)
            obj = obj@LearningAlgorithm(name);
            obj.params.C_l = C_l;
            obj.params.C_u = C_u;
            obj.params.s = s;
            obj.solver = solver;
        end
        
        function [obj, trainInfo] = train(obj, trainData, unlabeledData)
            
            % Initialize output weight vector
            w0 = zeros(size(trainData.X, 2), 1);
            
            % Translate the unlabeled data
            unlabeledData = translateUnlabeled(unlabeledData);
            
            % Set the offset
            obj.b = mean(trainData.Y);
            
            % Solve the inner optimization problem
            fcn = @(w) obj.computeObjFunction(trainData, unlabeledData, w);
            fcn_grad = @(w) obj.computeGradient(trainData, unlabeledData, w, size(trainData.X, 1), size(unlabeledData.X, 1));
            [obj.w, trainInfo] = obj.solver.solve(fcn, fcn_grad, w0);
            
        end
        
        function [grad, grad_f_new, grad_g_new, grad_h_new] = computeGradient(obj, trainData, unlabeledData, w, Nlabeled, Nunlabeled)
            % Compute the gradient of the objective function
            %   J(w) = f(w) + g(w) + h(w)
            % where:
            %    f(w) is the square hinge loss over labeled samples
            %    g(w) is the exponential approximation for unlabeled
            %    samples
            %    h(w) is the L2-norm regularizer
            % The function returns separately the gradients for each
            % function.
            
            if(nargin < 5)
                Nlabeled = size(trainData.X, 1);
                Nunlabeled = size(unlabeledData.X, 1);
            end
            
            yfx = trainData.Y.*(trainData.X*w + obj.b);
            aux_coefficient = (yfx < 1).*trainData.Y.*(1-yfx);

            y_unlabeled = unlabeledData.X*w + obj.b;
            aux2 = exp(-obj.params.s*y_unlabeled.^2).*y_unlabeled;
            
            grad_f_new = -2*(obj.params.C_l/Nlabeled)*sum(repmat(aux_coefficient, 1, size(trainData.X, 2)).*trainData.X, 1)';
            grad_g_new = -2*obj.params.s*(obj.params.C_u/Nunlabeled)*sum(unlabeledData.X.*repmat(aux2, 1,size(trainData.X,2)), 1)';
            grad_h_new = w;
            
            grad = grad_f_new + grad_g_new + grad_h_new;
             
        end
        
        function func = computeObjFunction(obj, trainData, unlabeledData, w)
            % Compute objective function (see help of computeGradient for
            % the definition of the objective function).

            y_labeled = trainData.X*w + repmat(obj.b, size(trainData.X,1),1);
            y_unlabeled = unlabeledData.X*w + repmat(obj.b, size(unlabeledData.X,1),1);
            
            aux = y_labeled.*trainData.Y;
            func = (norm(w)^2)/2 + (obj.params.C_l/size(trainData.X, 1))*sum(max(0, 1 - aux).^2) + (obj.params.C_u/size(unlabeledData.X, 1))*sum(exp(-obj.params.s*y_unlabeled.^2));

        end
        
        function [labels,scores] = test(obj, testData)
            % Run the trained SVM
            labels = sign(testData.X*obj.w + repmat(obj.b, size(testData.X,1),1));
            scores = labels;
        end
    end
    
end

