% SerialDataDistributedRVFL - Serialized version of a distributed RVFL
%   Refer to the following link for more information:
%	http://ispac.ing.uniroma1.it/scardapane/software/lynx/dist-learning/

% License to use and modify this code is granted freely without warranty to all, as long as the original author is
% referenced and attributed as such. The original author maintains the right to be solely associated with this work.
%
% Programmed and Copyright by Simone Scardapane:
% simone.scardapane@uniroma1.it

classdef DistributedRVFL < RandomVectorFunctionalLink
    
    properties
    end
    
    methods
        
        function obj = DistributedRVFL(name, neurons, lambda, varargin)
            obj = obj@RandomVectorFunctionalLink(name, neurons, lambda);
            p = inputParser();
            p.addParameter('consensus_max_steps', 300);
            p.addParameter('consensus_thres', 0.001);
            p.addParameter('admm_max_steps', 300);
            p.addParameter('admm_rho', 1);
            p.addParameter('admm_reltol', 0.001);
            p.addParameter('admm_abstol', 0.001);
            p.parse(varargin{:});
            obj.params.consensus_max_steps = p.Results.consensus_max_steps;
            obj.params.consensus_thres = p.Results.consensus_thres;
            obj.params.admm_max_steps = p.Results.admm_max_steps;
            obj.params.admm_rho = p.Results.admm_rho;
            obj.params.admm_reltol = p.Results.admm_reltol;
            obj.params.admm_abstol = p.Results.admm_abstol;
        end
        
        
        function [obj, info] = train(obj, net, trainData, ~)
            
            N_hidden = obj.B;
            N_nodes = net.N;
            
            obj = obj.generateWeights(size(trainData.X, 2));
            
            info = struct();
            
            idx = {':'};
            
            % Global term
            z = zeros(N_hidden, 1);
            
            % Lagrange multipliers
            t = zeros(N_hidden, N_nodes);
            
            % Parameters
            rho = obj.params.admm_rho;
            steps = obj.params.admm_max_steps;
            
            % Precompute the inverse matrices
            Hinv = cell(N_nodes, 1);
            HY = cell(N_nodes, 1);
            
            for ii = 1:N_nodes
                
                d_local = getLocalPart(trainData, ii);
                Xtr = d_local.X;
                
                Hinv{ii} = obj.computeHiddenMatrix(Xtr);
                HY{ii} = Hinv{ii}'*d_local.Y;
                
                if(size(Xtr, 1) > N_hidden)
                    Hinv{ii} = inv(eye(N_hidden)*rho + Hinv{ii}' * Hinv{ii});
                else
                    Hinv{ii} = (1/rho)*(eye(N_hidden) - Hinv{ii}'*inv(rho*eye(size(Xtr, 1)) + Hinv{ii}*Hinv{ii}')*Hinv{ii});
                end
                
                
            end
            
            beta = zeros(N_hidden, N_nodes);
            
            for ii = 1:steps
                
                for jj = 1:N_nodes
                    
                    % Compute current weights
                    beta(idx{:}, jj) = Hinv{jj}*(HY{jj} + rho*z - t(idx{:}, jj));
                    
                end
                
                % Run consensus
                [beta_avg, ~] = ...
                    obj.run_consensus_serial(net, beta, obj.params.consensus_max_steps, obj.params.consensus_thres);
                [t_avg, ~] = obj.run_consensus_serial(net, t, obj.params.consensus_max_steps, obj.params.consensus_thres);
                
                
                % Store the old z and update it
                zold = z;
                z = (rho*beta_avg + t_avg)/(obj.lambda/N_nodes + rho);
                
                % Compute the update for the Lagrangian multipliers
                for jj = 1:N_nodes
                    t(idx{:}, jj) = t(idx{:}, jj) + rho*(beta(idx{:}, jj) - z);
                end
                
                
            end
            
            obj.outputWeights = beta(idx{:}, 1);
            
        end
        
        function [final_value, consensus_error] = run_consensus_serial(obj, net, initial_values, steps, threshold)
            % This is a serial implementation of the consensus strategy.
            % Parameters are:
            %
            %   - INITIAL_VALUES: matrix of initial values. For consensus
            %   of d-dimensional vectors, this is a dxN matrix, where N is
            %   the total number of agents.
            %
            %   - STEPS: maximum number of iterations
            %
            %   - THRESHOLD: threshold for the disagreement norm. When this
            %   is the lower than the threshold on all nodes, consensus is
            %   ended.
            %
            % Output values are the computed average, and the average
            % evolution of the disagreement on the nodes.
            
            
            current_values = initial_values;
            consensus_error = zeros(steps, 1);
            
            for ii = 1:steps
                
                current_values = current_values*net.W;
                aux = current_values - repmat(mean(current_values, 2), 1, net.N);
                consensus_error(ii) = mean(diag(sqrt(aux'*aux)));
                if(consensus_error(ii) < threshold)
                    break;
                end
                
            end
            
            final_value = current_values(:, 1);
            
        end
        
    end
end

