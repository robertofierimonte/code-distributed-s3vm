classdef DiffusionNablaS3VM < GradientDescentSolver & NablaS3VM
    % Distributed NablaS3VM using diffusion gradient descent or NEXT 
    % framework.
    
    properties
        trainAlg;       % Train algorithm (can be 'distributedGrad' or 'next')
        internalSolver; % Internal solver for the w update
    end
    
    methods
        function obj = DiffusionNablaS3VM(name, C_l, C_u, s, varargin)
            obj = obj@GradientDescentSolver(varargin{:});
            obj = obj@NablaS3VM(name, C_l, C_u, s, []);
            p = inputParser;
            p.KeepUnmatched = true;
            p.addParamValue('trainAlg', 'distributedGrad');
            p.addParamValue('internalSolver', GradientDescentSolver('threshold', 10^-5, 'maxIters', 200, 'alpha0', 0.5, 'delta', 0.55));
            p.parse(varargin{:});
            obj.trainAlg = p.Results.trainAlg;
            obj.internalSolver = p.Results.internalSolver;
        end
        
        function [obj, trainInfo] = train(obj, net, trainData, unlabeledData)
            
            % Auxiliary variables
            L = net.N;
            d = size(trainData.X, 2);
            Nlabeled = size(trainData.X, 1);
            Nunlabeled = size(unlabeledData.X, 1);
            
            % Cell array for storing local datasets
            locTrainData = cell(L, 1);
            locUnlabeledData = cell(L,1);
            
            % Output structures
            fval = zeros(obj.maxIters, 1);
            gval = zeros(obj.maxIters, 1);
            disagreement = zeros(obj.maxIters, L);
            
            % Initialize local weight variables
            wCurr = zeros(size(trainData.X,2),L);
            
            % Translate unlabeled data
            unlabeledData = translateUnlabeled(unlabeledData);
            
            % Compute the offset
            obj.b = mean(trainData.Y);
            
            % Save the local parts of the datasets
            for ii = 1:L
                locTrainData{ii}= getLocalPart(trainData, ii);
                locUnlabeledData{ii} = getLocalPart(unlabeledData, ii);
            end

            if strcmp(obj.trainAlg, 'distributedGrad')
                
                for n = 1:obj.maxIters
                    
                    % Compute current step size
                    alpha = obj.alpha0/(n)^obj.delta;
                    
                    % Evaluate objective function and gradients
                    fval(n) = obj.computeObjFunction(trainData, unlabeledData, mean(wCurr, 2));
                    gval(n) = norm(obj.computeGradient(trainData, unlabeledData, mean(wCurr, 2), Nlabeled, Nunlabeled))^2;
                    
                    % Perform one gradient descent step
                    for ii = 1:L
                        [~, grad_f, grad_g, grad_h] = obj.computeGradient(locTrainData{ii}, locUnlabeledData{ii}, wCurr(:,ii), Nlabeled, Nunlabeled);
                        g = (grad_f + grad_g + grad_h/L);
                        wCurr(:,ii) = wCurr(:,ii) - alpha*g;
                    end

                    % Perform one consensus step
                    wCurr = wCurr*net.W;
                    
                    for ii = 1:L
                        disagreement(n, ii) = norm(wCurr(:, ii) - mean(wCurr, 2))^2;
                    end
                    
                    % Check termination condition
                    if (gval(n) <= obj.threshold)
                        break;
                    end
                end
            
            elseif strcmp(obj.trainAlg, 'next') 
                
                % Initialize variables for the gradients
                grad_f = zeros(d, L);
                grad_g = zeros(d, L);
                
                % Initialize the auxiliary variables vk and pi
                vCurr = zeros(d, L);
                piCurr = zeros(d, L);
                for k = 1:L
                    [~, grad_f(:, k), grad_g(:, k), ~] = obj.computeGradient(locTrainData{k}, locUnlabeledData{k}, wCurr(:,k), Nlabeled, Nunlabeled);
                    vCurr(:, k) = grad_f(:, k) + grad_g(:, k);
                    piCurr(:, k) = (L-1)*vCurr(:, k);
                end
                
                for n = 1:obj.maxIters
                    
                    psi = zeros(d, L);
         
                    alpha = obj.alpha0/(n)^obj.delta;

                    % Compute objective functions and gradient norms
                    % (for evaluation purposes)
                    fval(n) = obj.computeObjFunction(trainData, unlabeledData, mean(wCurr, 2));
                    gval(n) = norm(obj.computeGradient(trainData, unlabeledData, mean(wCurr, 2), Nlabeled, Nunlabeled))^2;
                    
                    for k = 1:L

                        % Solve inner optimization problem for wkhat[n]
                        obj_fcn_grad = @(w) obj.computeSurrogateGradient(locTrainData{k}, w, grad_g(:, k), piCurr(:, k), Nlabeled);
                        [w_hat, ~] = obj.internalSolver.solve([], obj_fcn_grad, wCurr(:, k));

                        % Update psi
                        dir = (w_hat - wCurr(:, k));
                        psi(:, k) = wCurr(:, k) + alpha*dir;
                        
                    end
                    
                    % Perform one consensus step on psi
                    wCurr = psi*net.W;
                    
                    vCurrNew = zeros(d, L);
                    for k=1:L
                        
                        % Update v
                        [~, grad_f_new, grad_g_new, ~] = obj.computeGradient(locTrainData{k}, locUnlabeledData{k}, wCurr(:,k), Nlabeled, Nunlabeled);
                        vCurrNew(:, k) = vCurr*net.W(:, k) + grad_f_new + grad_g_new - grad_f(:, k) - grad_g(:, k);

                        % Update pi
                        piCurr(:, k) = L*vCurrNew(:, k) - grad_f_new - grad_g_new;
                        
                        % Update the stored gradients
                        grad_f(:, k) = grad_f_new;
                        grad_g(:, k) = grad_g_new;
                        
                    end
                    
                    % Save new v
                    vCurr = vCurrNew;
                    
                    for k = 1:L
                        disagreement(n, k) = norm(wCurr(:, k) - mean(wCurr, 2))^2;
                    end
                    
                    % Check termination condition
                    if (gval(n) <= obj.threshold)
                        break;
                    end
                    
                end
           
                
            end

            obj.w = mean(wCurr, 2);
            
            trainInfo.fval = fval;
            trainInfo.gval = gval;
            trainInfo.disagreement = disagreement;
           
        end
        
        function grad = computeSurrogateGradient(obj, trainData, w, grad_g, pi, Nlabeled)
            % Compute gradient of surrogate function
            yfx = trainData.Y.*(trainData.X*w + obj.b);
            aux_coefficient = (yfx < 1).*trainData.Y.*(1-yfx);
            
            grad_f = -2*(obj.params.C_l/Nlabeled)*sum(repmat(aux_coefficient, 1, size(trainData.X, 2)).*trainData.X, 1)';
            grad_h = w;
 
            grad = (grad_f + grad_g + grad_h + pi);
            
        end
        
        function func = computeSurrogateFunction(obj, trainData, w, wCurr, grad_g, pi, Nlabeled)
            % Compute value of surrogate function
            y_labeled = trainData.X*w + obj.b;
            aux = y_labeled.*trainData.Y;
            
            func = (obj.params.C_l/Nlabeled)*sum(max(0, 1 - aux).^2) + ...
                grad_g'*(w-wCurr) + ...
                pi'*(w-wCurr) + ...
                ((w'*w)^2)/2;
        end

    end
    
end