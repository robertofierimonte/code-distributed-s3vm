classdef ConjugateGradientSolver
    % CONJUGATEOLVER - Conjugate gradient descent procedure for optimizing 
    % a generic function
    
    properties
        threshold;   % Threshold for the gradient norm
        maxIters;    % Maximum number of iterations
        wolfeParams; % Params for the Wolfe conditions
        betaStr;     % Strategy for choosing beta
    end
    
    methods
        
        function obj = ConjugateGradientSolver(varargin)
            p = inputParser;
            p.KeepUnmatched = true;
            p.addParamValue('threshold', 10^-5);
            p.addParamValue('maxIters', 500);
            p.addParamValue('c1', 10^-4, @(x) (0 < x) &&  (x < 1));
            p.addParamValue('c2', 0.9);
            p.addParamValue('alphaMax', 1);
            p.addParamValue('betaStr', 'PR+');
            p.parse(varargin{:});
            obj.threshold = p.Results.threshold;
            obj.maxIters = p.Results.maxIters;
            obj.wolfeParams.c1 = p.Results.c1;
            obj.wolfeParams.c2 = p.Results.c2;
            obj.wolfeParams.alphaMax = p.Results.alphaMax;
            obj.betaStr = p.Results.betaStr;
        end
        
        function [woptimal, info] = solve(obj, fcn, grad_fcn, w0)
            % Input arguments:
            %   - fcn: function to be minimized (only for debug purposes,
            %   can be empty).
            %   - grad_fcn: gradient of the function to be minimized.
            %   - w0: initial starting point.
            %
            % Output arguments:
            %   - woptimal: optimal value found by the procedure.
            %   - info: structure with debug information, namely 'fval' is
            %   a vector with function evaluations (only if 'fcn' is not
            %   empty), and gval a vector with gradient evaluations.
            fval = zeros(obj.maxIters,1);
            gval = zeros(obj.maxIters,1);
            
            wCurr = w0;
            
            currentGrad = grad_fcn(wCurr);
            dir = -currentGrad;
            for ii = 1:obj.maxIters
                if(norm(currentGrad) <= obj.threshold)
                    break;
                end
                
                % Evaluate objective function and gradient
                fval(ii) = fcn(wCurr);
                gval(ii) = norm(currentGrad).^2;
                
                % Compute the step size using the Wolfe conditions
                alpha = obj.strongWolfeSearch(fcn, grad_fcn, wCurr, dir);                
                
                % Update the solution
                wCurr = wCurr + alpha*dir;
                
                % Save the current grad in a temporary variable
                grad = grad_fcn(wCurr);
                
                % Update beta
                if strcmp(obj.betaStr, 'PR+')
                    beta = max(grad'*(grad - currentGrad)/norm(currentGrad)^2,0);                   
                elseif strcmp(obj.betaStr, 'PR')
                    beta = grad'*(grad - currentGrad)/norm(currentGrad)^2;
                elseif strcmp(obj.betaStr, 'FR')
                    beta = (grad'*grad)/(currentGrad'*currentGrad);
                end
                
                % Update the descent direction
                dir = -grad + beta*dir;
                
                % Update the gradient
                currentGrad = grad;               
                
            end
            
            woptimal = wCurr;
            
            info.fval = fval;
            info.gval = gval;
            
        end
        
        function alpha = strongWolfeSearch(obj, f, gradF, x, dir)

        % implementation of strong wolfe line search
        % 
        % original c++ code by m.siggel@dkfz.de
        % import to matlab by m.bangert@dkfz.de
        % modified by roberto.fierimonte@gmail.com
        % reference: nocedal: numerical optimization 3.4 line search methods
        %
        % f       - function handle of objective function
        % gradF   - function handle of gradient
        % x       - current iterate
        % dir     - search direction
        % slope0  - gradient of function at x in direction dir
        % alphaLo - alpha at lower objective function value for zoom
        % alphaHi - alpha at higher objectvie function value for zoom
        % of_0
        % oflo

            if obj.wolfeParams.c1 > obj.wolfeParams.c2
                error('c1 > c2\n');
            end
            
            alpha    = 1;

            alpha_0  = 0;
            alpha_1  = alpha;

            of = f(x);
            slope0 = gradF(x)'*dir;

            of_x = of;

            of_0 = of;
            iter = 0;

            while 1

                xc     = x+alpha_1*dir;
                of     = f(xc);
                slopec = gradF(xc)'*dir;

                % check if current iterate violates sufficient decrease
                if  (of > of_0 + slope0*obj.wolfeParams.c1*alpha_1) || ((of >= of_x ) && (iter > 0))
                    % there has to be an acceptable point between alpha_0 and alpha_1
                    % (because c1 > c2)
                    alpha = nocZoom(f, gradF, x, dir, slope0, alpha_0, alpha_1, of_0, of_x, obj.wolfeParams.c1, obj.wolfeParams.c2);
                    break;
                end
                % current iterate has sufficient decrease, but are we too close?
                if(abs(slopec) <= -obj.wolfeParams.c2*slope0)
                    % strong wolfe fullfilled, quit
                    alpha = alpha_1;
                    break;
                end
                % are we behind the minimum?
                if (slopec >= 0)
                    % there has to be an acceptable point between alpha_0 and alpha_1
                    alpha = nocZoom(f, gradF, x, dir, slope0, alpha_1, alpha_0, of_0, of, obj.wolfeParams.c1, obj.wolfeParams.c2);
                    break;
                end

                alpha_0 = alpha_1;
                %alpha_1 = min(alphaMax, alpha_1*3);
                alpha_1 = alpha_1 + (obj.wolfeParams.alphaMax - alpha_1)*rand(1);
                of_x = of;

                iter = iter + 1;               
            end
            
            function alpha = nocZoom(f, gradF, x, dir, slope0, alphaLo, alphaHi, of_0, ofLo, c1, c2)
            % this function is only called by mb_nocLineSearch - everything else does
            % not make sense!
                ii = 0;
                while 1

                    if ii == 10
                        alpha = 10^-4;
                        return;
                    end
                    alpha = (alphaLo+alphaHi)/2;
                    %alpha = quadInterpol(gradF, x, dir, alphaLo, alphaHi);
                    %alpha = cubicInterpol(f, gradF, x, dir, alphaLo, alphaHi);
                    xc    = x + alpha*dir;
                    of    = f(xc);

                    if of > of_0 + c1*alpha*slope0 || of >= ofLo
                        % if we do not observe sufficient decrease in point alpha, we set
                        % the maximum of the feasible interval to alpha
                        alphaHi = alpha;       
                    else
                        slopec = gradF(xc)'*dir;
                        % strong wolfe fullfilled?
                        if abs(slopec) <= -c2*slope0
                            return;
                        end
                        if slopec*(alphaHi-alphaLo) >= 0 % if slope positive and alphaHi > alphaLo  
                            alphaHi = alphaLo;
                        end
                        alphaLo = alpha;
                        ofLo    = of;
                    end
                    ii = ii + 1;
                end

            end
        end
    end
    
end

